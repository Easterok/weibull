"""weibull URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from accounts import views as accounts_views
from core import views as core_views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path

urlpatterns = [
    path('admin/', admin.site.urls),

    # user
    path('api/signup/', accounts_views.signup),
    path('api/login/', accounts_views.signin),
    path('api/logout/', accounts_views.signout),
    path('api/init/', accounts_views.init),
    path('api/activate/<uidb64>/<token>/', accounts_views.activate, name='activate'),

    # experiment
    path('api/experiment/', core_views.ExperimentListView.as_view()),
    path('api/experiment/create/', core_views.ExperimentRawCreateView.as_view()),
    path('api/experiment/<pk>/', core_views.ExperimentDetailView.as_view()),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
