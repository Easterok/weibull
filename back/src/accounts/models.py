from django.db import models
from django.contrib.auth.models import AbstractUser, Group


class User(AbstractUser):
    # bio
    bio = models.TextField(max_length=500, blank=True, null=True)

