import jwt
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from accounts.models import User


class UserCreateSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all(), message="Почта занята")]
    )
    username = serializers.CharField(
        validators=[UniqueValidator(queryset=User.objects.all(), message="Имя пользователя занято")]
    )
    password = serializers.CharField(min_length=8, write_only=True)

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')


class UserShortSerializer(serializers.ModelSerializer):
    token = serializers.SerializerMethodField()

    @staticmethod
    def get_token(obj):
        return jwt.encode({"sub": str(obj.pk)}, "secret").decode()

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'token')
