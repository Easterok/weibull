# Generated by Django 2.2.3 on 2019-12-22 17:39

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20191222_1719'),
    ]

    operations = [
        migrations.AddField(
            model_name='experiment',
            name='weibull_fit',
            field=django.contrib.postgres.fields.ArrayField(base_field=django.contrib.postgres.fields.ArrayField(base_field=models.FloatField(), size=None), null=True, size=None),
        ),
        migrations.DeleteModel(
            name='WeibullFit',
        ),
    ]
