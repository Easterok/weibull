from rest_framework import generics
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.permissions import IsAuthenticated

from core.models import Experiment
from core.serializers import ExperimentRawCreateSerializer, ExperimentSerializer, ExperimentDetailSerializer
from .utils.weibull import Weibull


def run_calc(pk, user_pk, measures):
    exp = Experiment.objects.get(pk=pk)

    weibull_instance = Weibull(measures, exp.weibull_correction)

    exp.weibull_fit = weibull_instance.get_fit()
    exp.w = weibull_instance.get_w()
    exp.s = weibull_instance.get_s()
    exp.limit_cross_section = weibull_instance.get_cross_section_by_pins()
    exp.limit_let = weibull_instance.get_let0()

    exp.save()


class ExperimentRawCreateView(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)
    parser_classes = (FormParser, MultiPartParser)
    serializer_class = ExperimentRawCreateSerializer

    def perform_create(self, serializer):
        instance = serializer.save()
        measures = instance.measures.values_list('fluence', 'see_count', 'let')

        run_calc(instance.pk, self.request.user.pk, measures)


class ExperimentListView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = ExperimentSerializer

    def get_queryset(self):
        queryset = Experiment.objects.all()

        if not self.request.user.is_staff:
            queryset = queryset.filter(author=self.request.user)

        return queryset


class ExperimentDetailView(generics.RetrieveDestroyAPIView):
    serializer_class = ExperimentDetailSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        queryset = Experiment.objects.all()

        if not self.request.user.is_staff:
            queryset = queryset.filter(author=self.request.user)

        return queryset
