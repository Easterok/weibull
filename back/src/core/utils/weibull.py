import json
import math

import numpy as np
import scipy.integrate as integrate
import scipy.optimize as optimize
import scipy.stats as stats
from scipy.stats import chi2

from ..models import ConfidenceIntervalCoeff

PINS = 32768
S_START = 0.1
S_END = 3
W_START = 10
W_END = 200
DEGREES_OF_FREEDOM = 4
CHI_PROB = 0.95

CHORD_C = 1e-6

results2 = [
    [480000, 131, 65],
    [97000, 23, 65],
    [420000, 107, 65],
    [720000, 162, 40],
    [760000, 139, 40],
    [4200000, 104, 17],
    [3900000, 123, 17],
    [20000000, 0, 7],
    [20000000, 0, 7],
]
results2_best_args = [37, 0.7, 16, 0.0000000117 * PINS]

modifiedRes = [
    [2.70e4, 121, 65],
    [7.00e4, 321, 65],
    [1.40e5, 461, 41],
    [1.80e5, 552, 41],
    [3.30e5, 446, 17],
    [2.40e5, 310, 17],
    [9.40e6, 3691, 7],
    [2.90e7, 7391, 7],
]

modifiedRes_best_args = [35, 1.3, 3, 1.52e-7 * PINS]

idealArgs = [
    [9.9e6, 1, 28.9],
    [7.95e6, 50, 40.73],
    [3.59e6, 100, 53.1],
    [2.5e6, 100, 57.6],
    [9.46e5, 100, 75.09],
    [4.23e5, 100, 106.2],
]


class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(NpEncoder, self).default(obj)


def clamp(value, min_value, max_value):
    return max(min_value, min(value, max_value))


class Weibull:
    def __init__(self, args, correction=1):
        self.args = args
        self._correction = correction

        s, w, let, limit_cross_section = self.get_properties()

        self._w = w
        self._s = s
        self._let0 = let
        self._limit_cross_section = limit_cross_section

    def cumulative(self, LETi, w, s, let0):
        return 1 - math.exp(-(((LETi - let0) / w) ** s))

    def get_fluence_fault(self, fluence):
        if fluence < 4.0e3:
            return 0.35
        if fluence < 3.9e5:
            return 0.28
        return 0.17

    def get_let0(self):
        return self._let0

    def get_minimum_let(self):
        min_let = np.inf

        for i in self.args:
            if i[1] > 0 and i[2] < min_let:
                min_let = i[2]

        return min_let

    def get_min_max_cross_section(self):
        cross_sections = []

        for arg in self.args:
            fluence = arg[0]
            sse_count = arg[1]

            if sse_count > 0:
                cross_section_median = (0.67 + sse_count) / fluence
                cross_sections.append(cross_section_median)

        return [np.min(cross_sections), np.max(cross_sections)]

    def get_cross_section_by_pins(self):
        return self._limit_cross_section / PINS

    def get_points_fault(self):
        result = []

        for arg in self.args:
            interval = ConfidenceIntervalCoeff.objects.filter(n=clamp(arg[1], 1, 1000), p=0.95)[::1][
                0]  # todo: change on const
            tl = interval.tl
            th = interval.th
            fluence_pogr = self.get_fluence_fault(arg[0])

            bottom_conf_int = tl * (0.67 + arg[1]) / ((1 + fluence_pogr) * arg[0]) / PINS
            top_conf_int = th * (0.67 + arg[1]) / ((1 - fluence_pogr) * arg[0]) / PINS

            result.append([bottom_conf_int, top_conf_int])

        return result

    def get_w(self):
        return self._w

    def get_s(self):
        return self._s

    def chords(self, x, let0):
        d_max = math.sqrt(2 * let0 + CHORD_C ** 2)

        def f1(x):
            return 1 - (2.37 * x) / (1.8 * CHORD_C + d_max)

        def f(x):
            return f1(CHORD_C) * ((d_max ** 3.8 - x ** 3.8) / (d_max ** 3.8 - CHORD_C ** 3.8)) ** 3 * (CHORD_C / x) ** 2

        if d_max >= x and x >= CHORD_C:
            return f(x)

        return f1(x)

    def diff_LET(self, let):
        return 1

    def freq(self, prop_w, prop_s, let_0, cross_section):
        max_let = sorted(self.args, key=lambda i: i[2], reverse=True)[0][2]

        def fn(let_w, let):
            return self.cumulative(let_w, prop_w, prop_s, let_0) * self.chords(CHORD_C * let_w / let,
                                                                               let_0) * self.diff_LET(let)

        S_i = 2 * (cross_section * cross_section + 2 * cross_section * CHORD_C)

        res = integrate.dblquad(fn, let_0, max_let, lambda x: let_0, lambda x: x)

        return S_i / 4 * res[0]

    def get_properties(self):
        all_argv = []

        def prod_probabilities(argv):
            s, w, let0, limit_cross_section = argv
            probs = []

            for arg in self.args:
                [fi, count, leti] = arg

                if leti > let0:
                    cumulative = self.cumulative(leti, w, s, let0)
                    _weibull = fi * limit_cross_section * cumulative
                    probability = stats.poisson.pmf(count, _weibull)

                    probs.append(probability)
            prod = np.prod(probs)

            if prod == 1.0:
                return 0.0

            all_argv.append([s, w, let0, limit_cross_section, prod])

            return -prod

        minimum_let = self.get_minimum_let()

        min_cross_section, max_cross_section = self.get_min_max_cross_section()

        bnds = optimize.Bounds([S_START, W_START, 0, max_cross_section],
                               [S_END, W_END, minimum_let, 10 * max_cross_section])

        result = optimize.differential_evolution(func=prod_probabilities, bounds=bnds)

        fun = -result.fun
        interval = chi2.ppf(CHI_PROB, DEGREES_OF_FREEDOM)
        freqs = []

        for i in all_argv:
            if i[4] > 0:
                i[4] = math.log(i[4] / fun)

                if i[4] >= -0.5 * interval:
                    s, w, let, cross_section, prob = i
                    freq = self.freq(w, s, let, cross_section)

                    freqs.append([s, w, let, cross_section, prob, freq])

        s, w, let, cross_section, prob, freq = sorted(freqs, key=lambda x: x[5], reverse=True)[0]

        return [s, w, let, cross_section]

    def get_fit(self):
        w = self._w
        s = self._s
        lets = np.arange(0, 200, 1)
        result_fit = []

        for let in lets:
            if let >= self._let0:
                cross_section = self._limit_cross_section * self.cumulative(let, w, s, self._let0) / PINS
                result_fit.append([let, cross_section])

        return result_fit


class ChartPoints:
    def get_points(self, arg):
        [fluence, count, let] = arg
        cross_section_median = (0.67 + count) / fluence / PINS
        interval = ConfidenceIntervalCoeff.objects.filter(n=clamp(count, 1, 1000), p=0.95)[::1][
            0]
        tl = cross_section_median - (cross_section_median * interval.tl / (1 + self.get_fluence_fault(fluence)))
        th = (cross_section_median * interval.th / (1 - self.get_fluence_fault(fluence))) - cross_section_median

        return {'tl': tl, 'th': th, 'median': cross_section_median, 'let': let, 'let_fail': 1}

    def format(self, point):
        return '{:0.2e}'.format(point)

    def get_fluence_fault(self, fluence):
        if fluence < 4.0e3:
            return 0.35
        if fluence < 3.9e5:
            return 0.28
        return 0.17
