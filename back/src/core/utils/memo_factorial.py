from .decoratos import memodict


@memodict
def memo_factorial(n):
    if n == 0:
        return 1
    else:
        return n * memo_factorial(n-1)
