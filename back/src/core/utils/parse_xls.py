import re

from openpyxl import load_workbook

from core.models import Measure


def parse_xls(file):
    measure_data = []
    wb = load_workbook(file, data_only=True)
    ws = wb['Результаты']

    KEY_MAP = {
        r'ион': 'ion',
        r'ЛПЭ': 'let',
        r'№.*образца': 'sample_number',
        r'^сеанс': 'exposure_session',
        r'подсеанс': 'exposure_subsession',
        r'^флюенс': 'fluence',
        r'ОРЭ': 'see_count',
        r'погрешность.*флюенса': 'fluence_inaccuracy'
    }

    collumn_numbers = {}
    header_row = None
    measures_start_line = None
    measures_finish_line = None

    for index, row in enumerate(ws.iter_rows(values_only=True)):
        if row[0] and not header_row:
            header_row = row

        if not row[0] and measures_start_line and not measures_finish_line:
            measures_finish_line = index
            break

        if row[1] == 'ИОНЫ' and not measures_start_line:
            measures_start_line = index + 2

    for index, header in enumerate(header_row):
        for key, value in KEY_MAP.items():
            if header and re.findall(key, header, flags=re.I | re.S):
                collumn_numbers[value] = index
                break

    for row_data in ws.iter_rows(min_row=measures_start_line, max_row=measures_finish_line, values_only=True):
        measure_params = {}

        for key, value in collumn_numbers.items():
            measure_params[key] = row_data[value]

        measure_data.append(Measure(**measure_params))

    return measure_data
