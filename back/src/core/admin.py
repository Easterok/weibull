from django.contrib import admin

from .models import ConfidenceIntervalCoeff

admin.site.register(ConfidenceIntervalCoeff)
