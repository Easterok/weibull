from django.contrib.postgres.fields import ArrayField
from django.db import models

from accounts.models import User


class Experiment(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    file_name = models.CharField(max_length=1024, null=True)
    name = models.CharField(max_length=1024, null=True)
    weibull_correction = models.FloatField(blank=True, default=1)
    weibull_fit = ArrayField(ArrayField(models.FloatField()), null=True)
    w = models.FloatField(null=True)
    s = models.FloatField(null=True)
    limit_cross_section = models.FloatField(null=True)
    limit_let = models.IntegerField(null=True)


class Measure(models.Model):
    experiment = models.ForeignKey(Experiment, on_delete=models.CASCADE, related_name='measures')
    sample_number = models.PositiveSmallIntegerField()
    exposure_session = models.PositiveIntegerField()
    exposure_subsession = models.PositiveIntegerField()
    ion = models.CharField(max_length=32)
    let = models.FloatField()
    fluence = models.FloatField()
    fluence_inaccuracy = models.FloatField()
    see_count = models.IntegerField()


class ConfidenceIntervalCoeff(models.Model):
    n = models.PositiveIntegerField()
    p = models.FloatField()
    th = models.FloatField()
    tl = models.FloatField()
