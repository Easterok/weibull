from django.core.validators import FileExtensionValidator
from rest_framework import serializers

from accounts.serializers import UserShortSerializer
from core.models import Experiment, Measure
from .utils.parse_xls import parse_xls
from .utils.weibull import ChartPoints


class MeasureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Measure
        fields = ('id', 'sample_number', 'exposure_session', 'ion', 'let', 'fluence', 'fluence_inaccuracy', 'see_count')


class ExperimentSerializer(serializers.ModelSerializer):
    author = serializers.HiddenField(default=serializers.CurrentUserDefault())
    measures = MeasureSerializer(many=True, write_only=True)

    class Meta:
        model = Experiment
        fields = ('id', 'author', 'file_name', 'measures', 'created_at', 'name')

    def create(self, validated_data):
        measures_data = validated_data.pop('measures')

        experiment = Experiment.objects.create(**validated_data)

        for measure_data in measures_data:
            Measure.objects.create(**measure_data, experiment=experiment)


class ExperimentRawCreateSerializer(serializers.ModelSerializer):
    author = serializers.HiddenField(default=serializers.CurrentUserDefault())
    measures = serializers.FileField(validators=[FileExtensionValidator(allowed_extensions=['xls', 'xlsx'])],
                                     write_only=True)

    class Meta:
        model = Experiment
        fields = ('file_name', 'author', 'measures', 'weibull_fit', 'name')

    def validate_measures(self, value):
        # try:
        measure_data = parse_xls(value)
        # except Exception as e:
        #     raise serializers.ValidationError('Bad file')

        return measure_data

    def create(self, validated_data):
        raw_measures_list = validated_data.pop('measures')
        file_name = self.initial_data['measures'].name

        experiment = Experiment.objects.create(**validated_data, file_name=file_name)

        for measure in raw_measures_list:
            measure.experiment = experiment

        Measure.objects.bulk_create(raw_measures_list)

        return experiment


class ExperimentDetailSerializer(serializers.ModelSerializer):
    measures = MeasureSerializer(many=True)
    author = UserShortSerializer()

    class Meta:
        model = Experiment
        fields = (
        'id', 'author', 'file_name', 'measures', 'weibull_fit', 'name', 'w', 's', 'limit_cross_section', 'limit_let')

    def to_representation(self, instance):
        ret = super(ExperimentDetailSerializer, self).to_representation(instance)
        updated_measures = []

        for measure in ret['measures']:
            items = list(measure.items())
            let = items[4][1]
            fluence = items[5][1]
            count = items[7][1]

            points = ChartPoints().get_points([fluence, count, let])
            updated_measures.append(points)

        ret['chart_points'] = updated_measures
        ret['limit_cross_section'] = '{:0.2e}'.format(ret['limit_cross_section'])
        ret['w'] = '{:0.4}'.format(ret['w'])
        ret['s'] = '{:0.3}'.format(ret['s'])

        return ret
