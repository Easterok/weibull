import {BehaviorSubject} from 'rxjs';

export class MockAuthService {
    isAuthorized$ = new BehaviorSubject<boolean>(false);

    setIsAuthorized(isAuthorized: boolean) {
        this.isAuthorized$.next(isAuthorized);
    }
}
