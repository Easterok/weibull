import {NgModule} from '@angular/core';
import {
    HTTP_INTERCEPTORS,
    HttpClientModule,
    HttpClientXsrfModule,
} from '@angular/common/http';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
    EKIT_DTO_CONVERTER_TOKEN,
    EkitDtoConverterService,
    WITH_CREDENTIALS_PROVIDER,
} from 'ngx-ekit';
import {AppComponent} from './app.component';
import {MenuModule} from './menu/menu.module';
import {AppRouterModules} from './router.module';
import {dtoValues} from './dto-values';
import {DtoConverterInterceptor} from './interceptors/dto-converter';
import {RequestFailedInterceptor} from './interceptors/request-failed.interceptor';
import {MatSnackBarModule} from '@angular/material';
import {registerLocaleData} from '@angular/common';
import localeRu from '@angular/common/locales/ru';

registerLocaleData(localeRu, 'ru');

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MenuModule,
        AppRouterModules,

        // http
        HttpClientModule,
        HttpClientXsrfModule.withOptions({
            cookieName: 'csrftoken',
            headerName: 'X-CSRFToken',
        }),
        MatSnackBarModule,
    ],
    bootstrap: [AppComponent],
    providers: [
        {provide: EKIT_DTO_CONVERTER_TOKEN, useValue: dtoValues},
        EkitDtoConverterService,
        WITH_CREDENTIALS_PROVIDER,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: DtoConverterInterceptor,
            multi: true,
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: RequestFailedInterceptor,
            multi: true,
        },
    ],
})
export class AppModule {}
