import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class WeibullService {
    constructor(private readonly http: HttpClient) {}

    start(data: FormData): Observable<any> {
        return this.http.post('/api/experiment/create/', data);
    }

    getExperiments(): Observable<any[]> {
        return this.http.get<any[]>('/api/experiment').pipe(
            map(results =>
                results
                    .sort(
                        ({createdAt: a}, {createdAt: b}) =>
                            new Date(b).valueOf() - new Date(a).valueOf(),
                    )
                    .map((result, index) => {
                        return {
                            ...result,
                            index: index + 1,
                        };
                    }),
            ),
        );
    }

    getExperimentDetail(id: number): Observable<any> {
        return this.http.get(`/api/experiment/${id}`);
    }
}
