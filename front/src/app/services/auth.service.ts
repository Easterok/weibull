import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    private readonly _isAuthorized$ = new BehaviorSubject<boolean>(null);

    constructor(private readonly http: HttpClient) {
        this.init().subscribe(({isAuthorized}) => this._isAuthorized$.next(isAuthorized));
    }

    get isAuthorized$(): Observable<boolean> {
        return this._isAuthorized$.asObservable();
    }

    init(): Observable<any> {
        return this.http.get('api/init/');
    }

    login(user: any): Observable<any> {
        return this.http
            .post('api/login/', user)
            .pipe(tap(() => this._isAuthorized$.next(true)));
    }

    join(user: any): Observable<any> {
        return this.http.post('api/signup/', user);
    }

    logout(): Observable<void> {
        return this.http
            .post<void>('api/logout/', null)
            .pipe(tap(() => this._isAuthorized$.next(false)));
    }
}
