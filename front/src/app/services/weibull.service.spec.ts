import {TestBed} from '@angular/core/testing';

import {WeibullService} from './weibull.service';

describe('WeibullService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: WeibullService = TestBed.get(WeibullService);
        expect(service).toBeTruthy();
    });
});
