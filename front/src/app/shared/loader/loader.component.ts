import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    HostBinding,
    Input,
    Output,
} from '@angular/core';

@Component({
    selector: 'app-loader',
    templateUrl: './loader.component.html',
    styleUrls: ['./loader.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoaderComponent {
    @Input()
    @HostBinding('class._event-blocker')
    showLoader = false;

    @Input()
    hasClose = false;

    @Input()
    information: string;

    @Input()
    diameter = 40;

    @Output() closeLoader = new EventEmitter<MouseEvent>();
}
