import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NonAuthZoneGuard} from './guards/non-auth-zone/non-auth-zone.guard';
import {AuthZoneGuard} from './guards/auth-zone/auth-zone.guard';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'login',
                loadChildren: () =>
                    import('./pages/login/login.module').then(m => m.LoginModule),
                canActivate: [NonAuthZoneGuard],
            },
            {
                path: 'join',
                loadChildren: () =>
                    import('./pages/join/join.module').then(m => m.JoinModule),
                canActivate: [NonAuthZoneGuard],
            },
            {
                path: 'form',
                loadChildren: () =>
                    import('./pages/result-form/result-form.module').then(
                        m => m.ResultFormModule,
                    ),
                canActivate: [AuthZoneGuard],
            },
            {
                path: 'results',
                loadChildren: () =>
                    import('./pages/results/results.module').then(m => m.ResultsModule),
                canActivate: [AuthZoneGuard],
            },
        ],
    },
    {
        path: '**',
        redirectTo: '/',
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRouterModules {}
