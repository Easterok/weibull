import {ChangeDetectionStrategy, Component} from '@angular/core';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';

class Link {
    constructor(public readonly url: string, private readonly title: string) {}

    toString(): string {
        return this.title;
    }
}

const links = [new Link('login', 'Вход'), new Link('join', 'Регистрация')];

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuComponent {
    readonly links = links;
    readonly isAuthorized$ = this.authService.isAuthorized$;

    constructor(
        private readonly authService: AuthService,
        private readonly router: Router,
    ) {}

    logout() {
        this.authService.logout().subscribe(() => this.router.navigate(['/']));
    }
}
