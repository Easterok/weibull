import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MenuComponent} from './menu.component';
import {MatButtonModule} from '@angular/material';
import {RouterModule} from '@angular/router';

const MAT_MODULES = [MatButtonModule];

@NgModule({
    declarations: [MenuComponent],
    imports: [CommonModule, MAT_MODULES, RouterModule],
    exports: [MenuComponent],
})
export class MenuModule {}
