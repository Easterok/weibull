import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {EkitInputFileModule} from 'ngx-ekit';
import {ResultFormComponent} from './result-form.component';
import {ResultFormRoutingModule} from './result-form-routing.module';
import {MatButtonModule, MatInputModule} from '@angular/material';
import {LoaderModule} from '../../shared/loader/loader.module';

const EKIT_MODULES = [EkitInputFileModule];
const MAT_MODULES = [MatButtonModule, MatInputModule];

@NgModule({
    declarations: [ResultFormComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        ResultFormRoutingModule,
        EKIT_MODULES,
        MAT_MODULES,
        LoaderModule,
    ],
    exports: [ResultFormComponent],
})
export class ResultFormModule {}
