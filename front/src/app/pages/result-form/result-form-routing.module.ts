import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ResultFormComponent} from './result-form.component';

const routes: Routes = [
    {
        path: '',
        component: ResultFormComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
})
export class ResultFormRoutingModule {}
