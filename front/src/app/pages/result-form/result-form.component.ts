import {ChangeDetectionStrategy, Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {WeibullService} from '../../services/weibull.service';
import {BehaviorSubject} from 'rxjs';

@Component({
    selector: 'app-result-form',
    templateUrl: './result-form.component.html',
    styleUrls: ['./result-form.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResultFormComponent {
    readonly excelMimeTypes = [
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    ];
    readonly showLoader$ = new BehaviorSubject<boolean>(false);

    readonly form = new FormGroup({
        file: new FormControl(null),
        limitCrossSection: new FormControl(null),
        limitLet: new FormControl(null),
        name: new FormControl(null, [Validators.required]),
    });

    constructor(private readonly weibullService: WeibullService) {}

    onSubmit() {
        this.form.markAllAsTouched();

        if (this.form.invalid) {
            return;
        }

        const {file, limitCrossSection, limitLet, name} = this.form.value;
        const formData = new FormData();

        formData.append('measures', file);
        formData.append('limit_cross_section', limitCrossSection);
        formData.append('limit_let', limitLet);
        formData.append('name', name);

        this.showLoader$.next(true);

        this.weibullService
            .start(formData)
            .subscribe({complete: () => this.showLoader$.next(false)});
    }
}
