import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    OnDestroy,
} from '@angular/core';
import {WeibullService} from '../../services/weibull.service';
import {takeUntilDestroy} from 'ngx-ekit';
import {BehaviorSubject} from 'rxjs';
import {animate, style, transition, trigger} from '@angular/animations';
import {FormControl} from '@angular/forms';

@Component({
    selector: 'app-results',
    templateUrl: './results.component.html',
    styleUrls: ['./results.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        trigger('detailExpand', [
            transition(':enter', [
                style({height: 0, opacity: 0}),
                animate('255ms ease-out', style({height: '*', opacity: 1})),
            ]),
            transition(':leave', [
                style({height: '*', opacity: 1}),
                animate('255ms ease-in', style({height: 0, opacity: 0})),
            ]),
        ]),
    ],
})
export class ResultsComponent implements OnDestroy {
    expandedElement = null;
    resultsDict: {[id: number]: any} = {};

    readonly searchControl = new FormControl();

    readonly experiments$ = new BehaviorSubject<any>([]);
    readonly displayedColumns = ['index', 'name', 'fileName', 'createdAt'];

    constructor(
        private readonly weibullService: WeibullService,
        private readonly cd: ChangeDetectorRef,
    ) {
        this.weibullService
            .getExperiments()
            .pipe(takeUntilDestroy(this))
            .subscribe(this.experiments$);
    }

    ngOnDestroy() {}

    changeExpandedElement(element: any) {
        this.expandedElement = this.expandedElement === element ? null : element;

        if (this.resultsDict[element.id]) {
            return;
        }

        this.weibullService
            .getExperimentDetail(element.id)
            .pipe(takeUntilDestroy(this))
            .subscribe(result => {
                this.resultsDict[element.id] = result;
                this.cd.markForCheck();
            });
    }
}
