import * as Chart from 'chart.js';
import {IPointWithFail} from './points.model';

Chart.defaults.pointsFailChart = Chart.defaults.bubble;

function drawLine(
    ctx: CanvasRenderingContext2D,
    x: number,
    y: number,
    toX: number,
    toY: number,
) {
    ctx.beginPath();
    ctx.moveTo(x, y);
    ctx.lineTo(toX, toY);
    ctx.stroke();
}

const strokeStyle = '#333333';

export const pointsFailChart = Chart.controllers.bubble.extend({
    draw: function(ease: any) {
        Chart.controllers.bubble.prototype.draw.call(this, ease);

        const ctx = this.chart.chart.ctx;
        const points = this._data;
        const yScale = this.chart.scales['y-axis-0'];
        const xScale = this.chart.scales['x-axis-0'];

        ctx.save();
        ctx.strokeStyle = strokeStyle;

        for (const point of points) {
            const {yFail, y, x, xFail} = point as IPointWithFail;
            const topPx = yScale.getPixelForValue(y - yFail);
            const bottomPx = yScale.getPixelForValue(y + yFail);
            const leftPx = xScale.getPixelForValue(x - xFail);
            const rightPx = xScale.getPixelForValue(x + xFail);
            const xPx = xScale.getPixelForValue(x);
            const yPx = yScale.getPixelForValue(y);

            // y fails
            drawLine(ctx, xPx, topPx, xPx, bottomPx);
            drawLine(ctx, xPx - 2, topPx, xPx + 2, topPx);
            drawLine(ctx, xPx - 2, bottomPx, xPx + 2, bottomPx);

            // x fails
            drawLine(ctx, leftPx, yPx, rightPx, yPx);
            drawLine(ctx, leftPx, yPx - 2, leftPx, yPx + 2);
            drawLine(ctx, rightPx, yPx - 2, rightPx, yPx + 2);
        }
    },
});
