import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ResultsPreviewComponent} from './results-preview.component';
import {MatButtonModule, MatChipsModule, MatDialogModule} from '@angular/material';
import {TableComponent} from '../table/table.component';
import {TableModule} from '../table/table.module';

@NgModule({
    declarations: [ResultsPreviewComponent],
    imports: [
        CommonModule,
        MatChipsModule,
        MatButtonModule,
        TableModule,
        MatDialogModule,
    ],
    exports: [ResultsPreviewComponent],
    entryComponents: [TableComponent],
})
export class ResultsPreviewModule {}
