export interface IPoint {
    x: number;
    y: number;
}

export interface IPointWithFail extends IPoint {
    yFail: number;
    xFail: number;
}
