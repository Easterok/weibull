import {
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    Input,
    ViewChild,
} from '@angular/core';
import * as Chart from 'chart.js';
import {IPoint, IPointWithFail} from './points.model';
import {pointsFailChart} from './points-fail-chart';
import {MatDialog} from '@angular/material';
import {TableComponent} from '../table/table.component';

Chart.controllers.pointsFailChart = pointsFailChart;

@Component({
    selector: 'app-results-preview',
    templateUrl: './results-preview.component.html',
    styleUrls: ['./results-preview.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResultsPreviewComponent {
    @Input() set detail(data: any) {
        this.details = data;
        this.measures = data.chartPoints.map(({let: x, median: y, tl, letFail}) => ({
            x,
            y,
            yFail: parseFloat(tl),
            xFail: letFail,
        }));
        this.weibullFit = data.weibullFit.map(([x, y]) => ({x, y}));
        this.maxYScale = Math.max(...this.measures.map(({yFail, y}) => yFail + y));
    }

    @ViewChild('chart', {static: false}) set setCanvas({
        nativeElement,
    }: ElementRef<HTMLCanvasElement>) {
        const canvasCtx = nativeElement.getContext('2d');

        this.canvas = new Chart(canvasCtx, {
            type: 'bubble',
            data: {
                datasets: [
                    {
                        label: 'Экспериментальные данные',
                        type: 'pointsFailChart',
                        data: this.measures,
                        borderColor: '#ff4400',
                        backgroundColor: '#ff4400',
                    },
                    {
                        label: 'Распределение Вейбулла',
                        type: 'line',
                        data: this.weibullFit,
                        pointRadius: 0,
                        borderWidth: 1,
                        backgroundColor: '#0066ff',
                        borderColor: '#0066ff',
                        fill: false,
                    },
                ],
            },
            options: {
                title: {
                    display: true,
                    text: 'Зависимость сечения ОРЭ от ЛПЭ',
                },
                scales: {
                    yAxes: [
                        {
                            ticks: {
                                suggestedMax: this.maxYScale,
                            },
                        },
                    ],
                },
            },
        });
    }

    details: any;
    private weibullFit: ReadonlyArray<IPoint>;
    private measures: ReadonlyArray<IPointWithFail>;
    private maxYScale: number;

    private canvas: any;

    constructor(private readonly dialog: MatDialog) {}

    openTablePopup() {
        this.dialog.open(TableComponent, {
            width: '600px',
            data: this.details.measures.map((data, index) => ({
                index: index + 1,
                ...data,
            })),
        });
    }
}
