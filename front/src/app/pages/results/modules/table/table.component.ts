import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
    selector: 'app-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableComponent {
    readonly displayedColumns = ['index', 'let', 'fluence', 'see_count'];

    constructor(
        public dialogRef: MatDialogRef<TableComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) {}
}
