import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ResultsComponent} from './results.component';
import {ResultsRouterModule} from './results-router.module';
import {MatInputModule, MatTableModule} from '@angular/material';
import {EkitLetModule} from 'ngx-ekit';
import {LoaderModule} from '../../shared/loader/loader.module';
import {ResultsPreviewModule} from './modules/results-preview/results-preview.module';
import {ReactiveFormsModule} from '@angular/forms';

const matModules = [MatTableModule];

@NgModule({
    declarations: [ResultsComponent],
    imports: [
        CommonModule,
        ResultsRouterModule,
        matModules,
        EkitLetModule,
        LoaderModule,
        ResultsPreviewModule,
        MatInputModule,
        ReactiveFormsModule,
    ],
})
export class ResultsModule {}
