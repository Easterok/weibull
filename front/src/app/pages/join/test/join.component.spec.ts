import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {AuthService} from '../../../services/auth.service';
import {JoinComponent} from '../join.component';
import {JoinModule} from '../join.module';

describe('JoinComponent', () => {
    let component: JoinComponent;
    let fixture: ComponentFixture<JoinComponent>;
    let joinSpy: jasmine.Spy;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [JoinModule, NoopAnimationsModule, HttpClientTestingModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(JoinComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    beforeEach(() => {
        const authService = TestBed.get(AuthService);

        joinSpy = spyOn(authService, 'join');
    });

    it('Компонент должен создаться', () => {
        expect(component).toBeTruthy();
    });

    it('Если форма не валидна, то метод join в AuthService не вызывается', () => {
        const form = component.form;

        form.patchValue({});
        component.onSubmit();

        expect(joinSpy).not.toHaveBeenCalled();
    });

    it('Если форма валидна, то метод join в AuthService вызывается', () => {
        const form = component.form;
        const user = {username: 'username', email: 'qwe@mail.ru', password: 'qwert12345'};

        form.patchValue({...user, passwordConfirm: 'qwert12345'});
        component.onSubmit();

        expect(joinSpy).toHaveBeenCalledWith(user);
    });
});
