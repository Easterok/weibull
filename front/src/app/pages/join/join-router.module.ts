import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {JoinComponent} from './join.component';

const routes = [
    {
        path: '',
        component: JoinComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
})
export class JoinRouterModule {}
