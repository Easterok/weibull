import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {BehaviorSubject, EMPTY} from 'rxjs';
import {catchError, finalize} from 'rxjs/operators';
import {
    EkitValidatorsError,
    emailValidator,
    matchingControls,
    maxLengthValidator,
    minLengthValidator,
    passwordValidator,
    requiredValidator,
    usernameValidator
} from 'ngx-ekit';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-join',
    templateUrl: './join.component.html',
    styleUrls: ['./join.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JoinComponent {
    readonly showLoader$ = new BehaviorSubject<boolean>(false);
    readonly form = this.fb.group(
        {
            username: [
                null,
                [
                    requiredValidator,
                    minLengthValidator(2),
                    maxLengthValidator(30),
                    usernameValidator,
                ],
            ],
            email: [null, [requiredValidator, maxLengthValidator(50), emailValidator]],
            password: [
                null,
                [
                    requiredValidator,
                    minLengthValidator(8),
                    maxLengthValidator(50),
                    passwordValidator,
                ],
            ],
            passwordConfirm: [
                null,
                [
                    requiredValidator,
                    minLengthValidator(8),
                    maxLengthValidator(50),
                    passwordValidator,
                ],
            ],
        },
        {
            validator: matchingControls(
                'Пароли не совпадают',
                'password',
                'passwordConfirm',
            ),
        },
    );

    constructor(
        private readonly fb: FormBuilder,
        private readonly cd: ChangeDetectorRef,
        private readonly authService: AuthService,
        private readonly router: Router,
    ) {}

    onSubmit() {
        if (this.form.invalid) {
            return;
        }

        const {passwordConfirm, ...user} = this.form.value;

        this.showLoader$.next(true);

        this.authService
            .join(user)
            .pipe(
                catchError(({error}) => {
                    this.setErrors(error);
                    this.cd.markForCheck();

                    return EMPTY;
                }),
                finalize(() => this.showLoader$.next(false)),
            )
            .subscribe(() => {
                this.router.navigate(['/login']);
            });
    }

    private setErrors(errors: {[key: string]: string[]}) {
        Object.keys(errors).forEach(key => {
            const control = this.form.get(key);

            if (control) {
                control.setErrors({
                    backError: new EkitValidatorsError(errors[key].join(', ')),
                });
            }
        });
    }
}
