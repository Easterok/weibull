import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {JoinComponent} from './join.component';
import {JoinRouterModule} from './join-router.module';
import {MatButtonModule, MatInputModule} from '@angular/material';
import {EkitControlErrorModule} from 'ngx-ekit';
import {ReactiveFormsModule} from '@angular/forms';
import {LoaderModule} from '../../shared/loader/loader.module';

const MAT_MODULES = [MatInputModule, MatButtonModule];
const EKIT_MODULES = [EkitControlErrorModule];

@NgModule({
    declarations: [JoinComponent],
    imports: [
        CommonModule,
        JoinRouterModule,
        ReactiveFormsModule,
        MAT_MODULES,
        EKIT_MODULES,
        LoaderModule,
    ],
})
export class JoinModule {}
