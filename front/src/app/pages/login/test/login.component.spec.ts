import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AuthService} from '../../../services/auth.service';
import {LoginComponent} from '../login.component';
import {LoginModule} from '../login.module';

describe('LoginComponent', () => {
    let component: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;
    let authService: AuthService;
    let loginSpy: jasmine.Spy;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [LoginModule, NoopAnimationsModule, HttpClientTestingModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LoginComponent);
        component = fixture.componentInstance;
        authService = TestBed.get(AuthService);
        loginSpy = spyOn(authService, 'login');
        fixture.detectChanges();
    });

    it('Компонент должен создаться', () => {
        expect(component).toBeTruthy();
    });

    it('Если в контроле username нет значения, то метод login AuthService не вызывается', () => {
        const control = component.form.get('username');

        control.setValue(null);
        component.onSubmit();

        expect(loginSpy).not.toHaveBeenCalled();
    });

    it('Если в контроле password нет значения, то метод login AuthService не вызывается', () => {
        const control = component.form.get('password');

        control.setValue(null);
        component.onSubmit();

        expect(loginSpy).not.toHaveBeenCalled();
    });

    it('Если в контролах есть значение и форма валидна, то метод login AuthService вызывается', () => {
        const user = {username: 'username', password: 'qwert1234'};

        component.form.patchValue(user);
        component.onSubmit();

        expect(loginSpy).toHaveBeenCalledWith(user);
    });
});
