import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatButtonModule, MatInputModule} from '@angular/material';
import {LoginComponent} from './login.component';
import {LoginRouterModule} from './login-router.module';
import {ReactiveFormsModule} from '@angular/forms';
import {EkitControlErrorModule} from 'ngx-ekit';
import {LoaderModule} from '../../shared/loader/loader.module';

const MAT_MODULES = [MatInputModule, MatButtonModule];
const EKIT_MODULES = [EkitControlErrorModule];

@NgModule({
    declarations: [LoginComponent],
    imports: [
        CommonModule,
        LoginRouterModule,
        ReactiveFormsModule,
        MAT_MODULES,
        EKIT_MODULES,
        LoaderModule,
    ],
})
export class LoginModule {}
