import {ChangeDetectionStrategy, Component} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {requiredValidator} from 'ngx-ekit';
import {AuthService} from '../../services/auth.service';
import {BehaviorSubject} from 'rxjs';
import {finalize} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent {
    readonly showLoader$ = new BehaviorSubject<boolean>(false);
    readonly form = this.fb.group({
        username: [null, requiredValidator],
        password: [null, requiredValidator],
    });

    constructor(
        private readonly fb: FormBuilder,
        private readonly authService: AuthService,
        private readonly router: Router,
        private readonly activatedRoute: ActivatedRoute,
    ) {}

    onSubmit() {
        if (this.form.invalid) {
            return;
        }

        const user = this.form.value;

        this.showLoader$.next(true);
        this.authService
            .login(user)
            .pipe(finalize(() => this.showLoader$.next(false)))
            .subscribe(() => {
                const url = this.activatedRoute.snapshot.queryParams.after || '/form';

                this.router.navigate([url]);
            });
    }
}
