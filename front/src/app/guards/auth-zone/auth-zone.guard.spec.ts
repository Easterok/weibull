import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {AuthZoneGuard} from './auth-zone.guard';
import {MockAuthService} from '../../test-utils/mocks/auth-service.mock';

describe('AuthZoneGuard', () => {
    let guard: AuthZoneGuard;
    let authService: MockAuthService;

    const destroy$ = new Subject<void>();

    beforeEach(() => {
        authService = new MockAuthService();
        guard = new AuthZoneGuard(authService as any);
    });

    afterEach(() => {
        destroy$.next();
        destroy$.complete();
    });

    it('Если пользователь не авторизован, то возвращает false', () => {
        authService.setIsAuthorized(false);

        guard
            .canActivate()
            .pipe(takeUntil(destroy$))
            .subscribe(value => {
                expect(value).toBe(false);
            });
    });

    it('Если пользователь авторизован, то true', () => {
        authService.setIsAuthorized(true);

        guard
            .canActivate()
            .pipe(takeUntil(destroy$))
            .subscribe(value => {
                expect(value).toBe(true);
            });
    });
});
