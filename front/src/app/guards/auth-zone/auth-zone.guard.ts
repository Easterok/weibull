import {Injectable} from '@angular/core';
import {CanActivate, Router, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from '../../services/auth.service';
import {filter, map} from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class AuthZoneGuard implements CanActivate {
    constructor(
        private readonly authService: AuthService,
        private readonly router: Router,
    ) {}

    canActivate(): Observable<boolean | UrlTree> {
        return this.authService.isAuthorized$.pipe(
            filter(auth => auth !== null),
            map(auth =>
                auth
                    ? auth
                    : this.router.createUrlTree(['/login'], {
                          queryParams: {
                              after: window.location.pathname,
                          },
                      }),
            ),
        );
    }
}
