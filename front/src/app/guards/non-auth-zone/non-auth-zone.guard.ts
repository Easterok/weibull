import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AuthService} from '../../services/auth.service';

@Injectable({
    providedIn: 'root',
})
export class NonAuthZoneGuard implements CanActivate {
    constructor(private readonly authService: AuthService) {}

    canActivate(): Observable<boolean> {
        return this.authService.isAuthorized$.pipe(map(isAuthorized => !isAuthorized));
    }
}
