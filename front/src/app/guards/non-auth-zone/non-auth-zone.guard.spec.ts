import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {NonAuthZoneGuard} from './non-auth-zone.guard';
import {MockAuthService} from '../../test-utils/mocks/auth-service.mock';

describe('NonAuthZoneGuard', () => {
    let guard: NonAuthZoneGuard;
    let authService: MockAuthService;

    const destroy$ = new Subject<void>();

    beforeEach(() => {
        authService = new MockAuthService();
        guard = new NonAuthZoneGuard(authService as any);
    });

    afterEach(() => {
        destroy$.next();
        destroy$.complete();
    });

    it('Если пользователь не авторизован, то возвращает true', () => {
        authService.setIsAuthorized(false);

        guard
            .canActivate()
            .pipe(takeUntil(destroy$))
            .subscribe(value => {
                expect(value).toBe(true);
            });
    });

    it('Если пользователь авторизован, то false', () => {
        authService.setIsAuthorized(true);

        guard
            .canActivate()
            .pipe(takeUntil(destroy$))
            .subscribe(value => {
                expect(value).toBe(false);
            });
    });
});
