import {EkitConverter} from 'ngx-ekit';

export const dtoValues: EkitConverter = {
    isAuthorized: 'is_authenticated',
    user: 'user_data',
    fileName: 'file_name',
    createdAt: 'created_at',
    weibullFit: 'weibull_fit',
    letFail: 'let_fail',
    limitCrossSection: 'limit_cross_section',
    limitLet: 'limit_let',
    chartPoints: 'chart_points',
};
