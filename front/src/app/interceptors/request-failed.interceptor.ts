import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {MatSnackBar} from '@angular/material';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';

const NOTIFICATION_DURATION = 3000;

export class RequestFailedInterceptor<T> implements HttpInterceptor {
    constructor(private readonly matSnackBar: MatSnackBar) {}

    intercept(req: HttpRequest<T>, next: HttpHandler): Observable<HttpEvent<T>> {
        return next.handle(req).pipe(
            catchError(error => {
                const message =
                    typeof error.error === 'string' ? error.error : error.statusText;

                this.matSnackBar.open(message, null, {
                    horizontalPosition: 'right',
                    verticalPosition: 'top',
                    duration: NOTIFICATION_DURATION,
                });

                return throwError(error);
            }),
        );
    }
}
